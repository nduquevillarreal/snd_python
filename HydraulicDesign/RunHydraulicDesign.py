# coding=utf-8
"""
@file
@author Natalia Duque
@section LICENSE

Sewer Networks Design (SND)
Copyright (C) 2016  CIACUA, Universidad de los Andes, Bogotá, Colombia

This program is a free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from HydraulicDesign.DataHandler import DataHandler
from HydraulicDesign.LayoutGraphbuilder import LayoutGraphbuilder
from HydraulicDesign.DesignGraphBuilder import DesignGraphBuilder
import os
# import cProfile, pstats, cStringIO
from pyinstrument import Profiler


class RunHydraulicDesign(object):

    def __init__(self):
        self.name = " Run hydraulic design"

    @staticmethod
    def run():
        # Create a profile
        # p1 = cProfile.Profile()
        # p1.enable()

        pr = Profiler()
        pr.start()

        # File with the solution of the Layout Selection problem
        # file = "..\Files\Results.txt"
        file = "..\Files\Basin_1.txt"
        # file = "..\Layout_Selection\Resultados.txt"

        p = os.path.abspath(file)

        # Read the file and create manholes and sections
        d = DataHandler(p)
        d.read_file()

        # Generate layout graph
        lg = LayoutGraphbuilder(d)
        print(lg.name)
        lg.build()

        # Generate and solve the hydraulic design graph
        gb = DesignGraphBuilder(d)
        print(gb.name)
        gb.build_and_solve()

        # Profiling the code
        with open("..\Files\profile2.txt", "w") as f:
            pr.stop()

            print(pr.output_html)
            print(pr.output_text(unicode=True, color=True))
            # f = open("..\Files\profile.txt", "w")
            f.write(str(pr.output_text()))



            # ps.disable()
            # s = cStringIO.StringIO()
            # ps = pstats.Stats(p1, stream=s).sort_stats("cumulative")
            # ps.print_stats()
            # ps.calc_callees()
            # print(s.getvalue())
            # f.write(str(s.getvalue()))

            # f.close()


        print("FINISHED------------------------------------------------------")


RunHydraulicDesign.run()
