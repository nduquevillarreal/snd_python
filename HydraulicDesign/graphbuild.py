# coding=utf-8
"""
@file
@author Natalia Duque
@section LICENSE

Sewer Networks Design (SND)
Copyright (C) 2016  CIACUA, Universidad de los Andes, Bogotá, Colombia

This program is a free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from HydraulicDesign import DesignHydraulics, LayoutNode, DesignNode, DesignGraphBuilder

import HydraulicDesign.DataHandler  as dh
from numpy import infty

def get_solution():

    solution=[]

    for m in dh.manholes:
        list_ls_nodes = m.layout_nodes

        for ls_node in list_ls_nodes:
            # Start from each external (initial) node towards the outfall
            if ls_node.type != "I":
                continue

            min_cost = infty
            min_cost_node = None
            # Get the minimum cost node (from the hydraulic design problem)
            for current_node in ls_node.nodes:

                if current_node is None:
                    continue
                cumulative_cost = current_node.Vi

                if cumulative_cost > min_cost:
                    continue
                min_cost = cumulative_cost
                min_cost_node = current_node

            parent_node = min_cost_node
            # Get all the parental nodes that belong to the shortest file_path
            if parent_node is None:
                continue

            antiguo = parent_node

            parent_node = parent_node.Pj

            if parent_node is not None:

                # print(" predecesor: "+parent_node.parental_loNode)
                # print(parent_node)
                # print("Layout Node "+ "CotaCorona "+ "CotaBatea "+ "CostoTramo "+ "pendiente "+ "longitud "+"di�metro "+ "yn "  +"�rea "+ "per�metro "+ "radio "+ "velocidad "+ "tao " +"froude " + "caudal ")

                for arc in self.designedArcs:
                    up= arc.up
                    down=arc.down

                    if antiguo == up and parent_node == down:
                        solution.add(arc)

#									double crownElevationA = down.elevation+down.diameter
#									System.out.println(down.parental_loNode.id+  " "+crownElevationA+" "+down.elevation+" " + actual.arcCost + " "+actual.slope+" "+actual.lenght+" " +actual.down.diameter +" "+actual.yn+" "+actual.area+" "+actual.perimeter+" "+actual.radius+" "+actual.speed+" "+actual.tao+" "+actual.froude+" "+actual.flow)


    for (int i = 0 i < solution.size() i++):
        for (int j = i+1 j< solution.size() j++):
            Designed_Arc sec_i = solution.get(i)
            Designed_Arc sec_j = solution.get(j)
            if(sec_i.parent_Layout_section.parent_section.equals(sec_j.parent_Layout_section.parent_section)):
                if(sec_i.up.elevation>=sec_j.up.elevation):
                    solution.remove(i)
                    j=i
                else:
                    solution.remove(j)
                    j--


p = r"E:\002_PERSONAL\10_PROGRAMAS TESIS\PY_SND\SND_V_FINAL (DW)\XPRESS\Resultados.txt"
d = DataHandler.DataHandler(input_data_path=p, n_elevation_change=100, n_min_depth=1.2, n_max_depth=5)
d.read_file()
d.get_manholes()
d.get_sections()
gb = DesignGraphBuilder.DesignGraphBuilder(data=d, roughness= 0.0000015, elevation_change= 100.0 )
gb.build_and_solve()
