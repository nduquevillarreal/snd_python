# coding=utf-8
"""
@file
@author Natalia Duque
@section LICENSE

Sewer Networks Design (SND)
Copyright (C) 2016  CIACUA, Universidad de los Andes, Bogotá, Colombia

This program is a free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from gurobipy import Model, GRB, multidict, quicksum

'''
Input data: 
Number of manholes 
Topology (Connectivity between manholes)
Costs(weight)
Inflow per manhole b[]
'''

manholes = 10    # Set the number of manholes
pipe_type = [1, 2]      # Type 1: outer-branch  2: inner-branch
M = 999999999999        # BIG number

results = open("..\Files\Results_test.txt", "w")

# Definition of arcs and weight of each arc
# The wight in both directions (i,j) or (j,i) is the same
# and is independent of the type of the pipe
# from the linear regression we obtain the slope m and the intercept b
arcs, slp, inter = multidict({
    (0, 1): (1, 1),
    (1, 2): (1, 1),
    (0, 3): (1, 1),
    (3, 4): (1, 1),
    (1, 4): (1, 1),
    (4, 5): (1, 1),
    (2, 5): (1, 1),
    (3, 6): (1, 1),
    (6, 7): (1, 1),
    (4, 7): (1, 1),
    (7, 8): (1, 1),
    (5, 8): (1, 1),
    (8, 9): (1, 1),
    (1, 0): (1, 1),
    (2, 1): (1, 1),
    (3, 0): (1, 1),
    (4, 3): (1, 1),
    (4, 1): (1, 1),
    (5, 4): (1, 1),
    (5, 2): (1, 1),
    (6, 3): (1, 1),
    (7, 6): (1, 1),
    (7, 4): (1, 1),
    (8, 7): (1, 1),
    (8, 5): (1, 1),
    (9, 8): (1, 1),
})

# Inflow per manhole. The last manhole receives the sum of all inflows with a negative value
inflow = [1, 1, 1, 1, 1, 1, 1, 1, 1, -9]

# Create a new model
# MIP model to solve the Layout Selection problem
m = Model("Layout Selection")

# Print summary
m.setParam('OutputFlag', 0)

'''
Decision variables
'''
# Binary decision variable representing the flow direction between adjacent manholes
c = 0
x = {(i, j, t): m.addVar(vtype=GRB.BINARY, name="x_" + str((i, j, t))) for i, j in arcs for t in pipe_type}

# Continuous variable representing the flow rate in each pipe
y = {(i, j, t): m.addVar(vtype=GRB.CONTINUOUS, name="y_" + str((i, j, t))) for i, j in arcs for t in pipe_type}


'''
#Constraints
'''
# Mass balance in the nodes
# Flow In - Flow Out = Storage in the node
m.addConstrs((quicksum(y[i, j, t] for i, j in arcs.select(i, '*') for t in pipe_type) - quicksum(
    y[k, i, t] for k, i in arcs.select('*', i) for t in pipe_type) == inflow[i]) for i in range(manholes))

# Lower bound for the flow rate in each pipe
for i, j in arcs:
    for t in pipe_type:
        m.addConstr((x[i, j, t] * (inflow[i])) <= 4.0 * y[i, j, t])  # the inflow is divided in the 4 assuming 4 adjacent manholes

# Upper bound for the flow rate in each pipe
for i, j in arcs:
    for t in pipe_type:
        m.addConstr(y[i, j, t] <= M * x[i, j, t])  # M is a BIG number (no upper limit)

# There is only one pipe per section of type t going in a specific direction i,j or j,i
for i, j in arcs:
    m.addConstr(quicksum(x[i, j, t] + x[j, i, t] for t in pipe_type) == 1)

# At most one inner-branch can come out from each manhole
for i in range(manholes):
    if i < manholes - 1:
        m.addConstr((quicksum(x[i, j, 2] for i, j in arcs.select(i, '*')) <= 1))

# Connectivity constraint: ensures that outer-branch and inner-branch pipes always drain into inner-branch pipes
# By definition, an inner-branch pipe cannot drain into an outer-branch pipe
for i in range(manholes):
    if i < manholes - 1:
        m.addConstr(quicksum(x[j, i, t] for j, i in arcs.select('*', i) for t in pipe_type) <= M * quicksum(
            x[i, k, 2] for i, k in arcs.select(i, '*')))
        m.addConstr(quicksum(x[j, i, t] for j, i in arcs.select('*', i) for t in pipe_type) >= quicksum(
            x[i, k, 2] for i, k in arcs.select(i, '*')))

# Maximum flow to be transported by an outer-branch pipe as the inflow coming from the upstream manhole
for i in range(manholes):
    if i < manholes - 1:
        m.addConstr(quicksum(y[i, j, 1] for i, j in arcs.select(i, '*')) <= inflow[i])

# The outfall pipe must be an outer-branch pipe towards the outfall (last manhole)
m.addConstr(x[manholes - 2, manholes - 1, 2] == 1)

# The outflow has to be equal to the sum of all inflows
total_flow = 0
for k in inflow:
    if k != inflow[len(inflow) - 1]:
        total_flow += k
m.addConstr(y[manholes - 2, manholes - 1, 2] == total_flow)


'''
Objective function
'''
# Set objective
c = {}
for i, j in arcs:
    for k in pipe_type:
        if x[i, j, k] is None:
            break
        else:

            # print("slp", s, "int", f)
            c[i, j] = slp[i, j]*y[i, j, k] + inter[i, j]*x[i, j, k]


z = quicksum(c[i, j] for i, j in arcs.select("*", "*"))

m.setObjective(z, GRB.MINIMIZE)
m.optimize()

print(c[i, j] for i, j in arcs.select("*", "*"))



if m.status != GRB.Status.OPTIMAL:
    print("Error: fixed model isn't optimal")
    exit(1)
else:
    print("Status: OPTIMAL")
    # for v in m.getVars():
    #     print('%s %g' % (v.varName, v.x))

    # print(m.printStats())
    print('Obj: %g' % m.objVal)

results.write("Manholes " + str(manholes) + "\n")
# for i in range(manholes):
#     results.write(str(x[i, j, t].x) + "\n")
#     print(str(i) + " " + str(j) + " " + str(t) + " " + str(y[i, j, t].x))

results.write("Sections " + str(len(arcs)/2) + "\n")
for i, j in arcs:
    for t in pipe_type:
        if x[i, j, t].x == 1:
            results.write(str(i) + " " + str(j) + " " + str(t) + " " + str(y[i, j, t].x) + "\n")
            print(str(i) + " " + str(j) + " " + str(t) + " " + str(y[i, j, t].x))
#
# for i, j in arcs:
#     for t in pipe_type:
#
#         print(x[i, j, t])



#
# print(m.getObjective())
# print(m.printStats())
# print(m.objVal)

